<?php
namespace Home\Controller;
use Think\Controller;

class IndexController extends Controller {

    public function index(){

        $blog  = $this->getBlogModel();
        $count = $blog->order('c_time')->count();
        $page  = new \Think\Page($count,4);

        $blogList = $blog->order('c_time desc')->limit($page->firstRow.',',$page->listRows)->select();
        
        $page->setConfig('prev','上一页');
        $page->setConfig('next','下一页');
        $page->setConfig('first','首页');
        $page->setConfig('last','尾页');
        
        $show = $page->show();
        $this->assign('blist',$blogList);
        $this->assign('show',$show);
    	$this->display();

    }

    public function add(){

        $this->display();
    }

    public function doAdd(){

        $data['title']     = I('post.title');
        $data['intro']     = I('post.intro');
        $data['content']   = I('post.content');
        $data['c_time']    = time();

        $blog = M('Blog');
        if ($blog->create($data)){
            $res  = $blog->add();
            if ($res){
                $this->success('ok!','index');
            }else{
                $this->error('fail!','index');
            }
        }

    }


    private function getBlogModel(){

        $blog = M('Blog');
        return $blog;

    }

    public function viewBlog(){

        require "./Public/comm/Parsedown.php";
         
        $id = I("get.id");
        if(!is_numeric($id)){
            $this->error('wrong');
        }
        
        $blog   = $this->getBlogModel();
        $b_info = $blog->where('id='.$id)->find();
        $b_info['content'] = $Parsedown->text(htmlspecialchars_decode($b_info['content']));
        
        if(!isset($_SESSION['view'.$id])){
            $_SESSION['view'.$id] = true;
            $blog->readnums += 1;
            $_SESSION['blog_id'] = $id;
            $blog->where('id='.$id)->save(); 
        }

        $current_time = $blog->where('id ='.$id)->getField('c_time');
        
        $last = $blog->where("c_time > ".$current_time)->limit(1)->order('c_time asc')->select();
        $next = $blog->where("c_time < ".$current_time)->limit(1)->order('c_time desc')->select();
    
        $this->assign('last',$last['0']);
        $this->assign('next',$next['0']);
        $this->assign('b_info',$b_info);
        $this->display();
        

    }

    public function aa(){

        $test = M('Test');
        $test->good();

    }
}