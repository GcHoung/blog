<?php
/**
 * Created by PhpStorm.
 * User: Gcx
 * Date: 2015/7/19
 * Time: 20:50
 */

/*
 * check username
 * */

function check_uname($uname)
{
    $uname = trim($uname);
    if (mb_strlen($uname,'utf-8') < 3 || mb_strlen($uname,'utf-8') > 20)
    {
        message_show("用户名长度不正确！",'');
    }

    $pattern = '/^[a-zA-Z_\x{4e00}-\x{9fa5}][\x{4e00}-\x{9fa5}\w]/u';
    if (!preg_match($pattern,$uname))
    {
        message_show("用户名格式不正确！",'');
    }

    return $uname;
}

/*
 * check password
 */

function check_password($password)
{
    $password = trim($password);
    if (strlen($password) < 6 || strlen($password) > 28)
    {
        message_show('密码长度不正确！','');
    }

    return $password;
}

/*
 * check the email
 */
function check_email($email)
{
    $email = trim($email);
    $pattern = '/^[a-z0-9]([a-z0-9]*[-_]?[a-z0-9]+)*@([a-z0-9]*[-_]?[a-z0-9]+)+[\.][a-z]{2,3}([\.][a-z]{2})?$/i';
    if (!preg_match($pattern,$email))
    {
        message_show("邮箱格式不正确！",'');
    }

    return $email;
}

/**
 *check title
 */
function checkTitle($title)
{
    $title = trim($title);
    if (mb_strlen($title,'utf-8') < 2 || mb_strlen($title,'utf-8') > 30)
    {
        message_show("标题长度不正确！",'');
    }

    $pattern = '/^[a-zA-Z_\x{4e00}-\x{9fa5}\w][\x{4e00}-\x{9fa5}\w]/u';
    if (!preg_match($pattern,$title))
    {
        message_show("标题格式不正确！",'');
    }

    return $title;
}